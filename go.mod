module gitlab.com/mkim-deem/suggestion

go 1.17

require github.com/google/uuid v1.3.0

require github.com/go-echarts/go-echarts/v2 v2.2.3

//dev mode
replace github.com/go-echarts/go-echarts/v2 => /Users/mkim/go/src/github.com/go-echarts
