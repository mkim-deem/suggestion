package main

// refs:
// Graph structure
//	- https://fodor.org/blog/go-graph/#:~:text=Unlike%20the%20queue%2C%20the%20stack,have%20weights%20associated%20to%20them.
// go-ecahrts
//	- https://github.com/go-echarts/go-echarts
//	- https://github.com/go-echarts/examples

// read later:
// Interface
//	- https://forum.golangbridge.org/t/when-to-use-a-function-which-receives-a-pointer-to-interface/14484/2

import (
	"fmt"
	"time"
)

const (
	//layoutISO = "2006-01-02T15:04:05.000Z"
	layoutISO = "2006-01-02T15:04:05"
)

const (
	tab     = "\t"
	newline = "\n"
)

func main() {
	fmt.Println("---------------------- [make a schedule set] ----------------------")

	scheduleSet := make(ScheduleSet)

	// Example 1
	/*
		flightScheduleA := NewFlightSchedule(
			"FlightA",
			time.Date(2022, 1, 9, 22, 0, 0, 0, time.UTC),
			time.Date(2022, 1, 10, 7, 0, 0, 0, time.UTC),
		)

		hotelSchedule := NewHotelSchedule(
			"Hotel",
			time.Date(2022, 1, 10, 0, 0, 0, 0, time.UTC),
			time.Date(2022, 1, 20, 0, 0, 0, 0, time.UTC),
		)

		flightScheduleB := NewFlightSchedule(
			"FlightB",
			time.Date(2022, 1, 20, 13, 0, 0, 0, time.UTC),
			time.Date(2022, 1, 20, 15, 0, 0, 0, time.UTC),
		)

		scheduleSet.AddSchedule(flightScheduleA)
		scheduleSet.AddSchedule(hotelSchedule)
		scheduleSet.AddSchedule(flightScheduleB)
	*/

	// Example 2
	hotelSchedule := NewHotelSchedule(
		"Hotel",
		time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC),
		time.Date(2022, 1, 10, 0, 0, 0, 0, time.UTC),
	)

	customSchedule1 := NewCustomSchedule(
		"Conference",
		time.Date(2022, 1, 3, 13, 0, 0, 0, time.UTC),
		time.Date(2022, 1, 3, 16, 0, 0, 0, time.UTC),
	)

	customSchedule2 := NewCustomSchedule(
		"Dinner with Client",
		time.Date(2022, 1, 5, 17, 0, 0, 0, time.UTC),
		time.Date(2022, 1, 5, 20, 0, 0, 0, time.UTC),
	)

	flightSchedule := NewFlightSchedule(
		"Flight",
		time.Date(2022, 1, 10, 23, 0, 0, 0, time.UTC),
		time.Date(2022, 1, 11, 6, 0, 0, 0, time.UTC),
	)

	scheduleSet.AddSchedule(hotelSchedule)
	scheduleSet.AddSchedule(customSchedule1)
	scheduleSet.AddSchedule(customSchedule2)
	scheduleSet.AddSchedule(flightSchedule)

	fmt.Println("done")

	fmt.Println("---------------------- [make a graph from schedule set] ----------------------")

	graph := NewGraph()
	graph.Init(&scheduleSet)
	fmt.Println("done")

	fmt.Println("---------------------- [make suggestions] ----------------------")

	graph.FilterEdgesForSuggestions()
	fmt.Println("done")

	fmt.Println("---------------------- [make a chart from graph] ----------------------")

	// Q: this doesn't work
	//chart := graphCircle() //graphCircleFrom(graph)
	//f, err := os.Create("result.html")
	//if err != nil {
	//	panic(err)
	//
	//}
	//chart.Render(f)

	GraphExamples{}.Examples(graph)
	fmt.Println("result.html has been created")
	fmt.Println("done")
}
