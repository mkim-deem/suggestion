package main

import (
	"fmt"
	"github.com/google/uuid"
	"time"
)

type ScheduleID uuid.UUID
type ScheduleType int

const (
	Flight    ScheduleType = 0
	Hotel                  = 1
	CarRental              = 2
	Custom                 = 3
	Unknown                = 4
)

type Schedule interface {
	ID() ScheduleID
	Alias() string
	StartTime() time.Time
	EndTime() time.Time
	ScheduleType() ScheduleType
}

// Q: interface can not be a receiver. how can I make common function of interface?
//func (s Schedule) String() string {
//	return "common log function"
//}

type FlightSchedule struct {
	id        ScheduleID
	alias     string
	startTime time.Time
	endTime   time.Time
}

func NewFlightSchedule(alias string, startTime, endTime time.Time) *FlightSchedule {
	return &FlightSchedule{
		id:        ScheduleID(uuid.New()),
		alias:     alias,
		startTime: startTime,
		endTime:   endTime,
	}
}

// Q: why I can't make it n *FlightSchedule?
func (s FlightSchedule) String() string {
	// short
	return fmt.Sprintf(
		"%s",
		s.Alias(),
	)

	// long
	//return fmt.Sprintf(
	//	"[%s, %s ~ %s]",
	//	s.Alias(),
	//	s.startTime.Format(layoutISO),
	//	s.endTime.Format(layoutISO),
	//)
}

func (s *FlightSchedule) ID() ScheduleID {
	return s.id
}

func (s *FlightSchedule) Alias() string {
	return s.alias
}

func (s *FlightSchedule) StartTime() time.Time {
	return s.startTime
}

func (s *FlightSchedule) EndTime() time.Time {
	return s.endTime
}

func (s *FlightSchedule) ScheduleType() ScheduleType {
	return Flight
}

type HotelSchedule struct {
	id        ScheduleID
	alias     string
	startTime time.Time
	endTime   time.Time
}

func NewHotelSchedule(alias string, startTime, endTime time.Time) *HotelSchedule {
	return &HotelSchedule{
		id:        ScheduleID(uuid.New()),
		alias:     alias,
		startTime: startTime,
		endTime:   endTime,
	}
}

func (s HotelSchedule) String() string {
	// short
	return fmt.Sprintf(
		"%s",
		s.Alias(),
	)

	// long
	//return fmt.Sprintf(
	//	"[%s, %s ~ %s]",
	//	s.Alias(),
	//	s.startTime.Format(layoutISO),
	//	s.endTime.Format(layoutISO),
	//)
}

func (s *HotelSchedule) ID() ScheduleID {
	return s.id
}

func (s *HotelSchedule) Alias() string {
	return s.alias
}

func (s *HotelSchedule) StartTime() time.Time {
	return s.startTime
}

func (s *HotelSchedule) EndTime() time.Time {
	return s.endTime
}

func (s *HotelSchedule) ScheduleType() ScheduleType {
	return Hotel
}

type CarRentalSchedule struct {
	id        ScheduleID
	startTime time.Time
	endTime   time.Time
}

type CustomSchedule struct {
	id        ScheduleID
	alias     string
	startTime time.Time
	endTime   time.Time
}

func NewCustomSchedule(alias string, startTime, endTime time.Time) *CustomSchedule {
	return &CustomSchedule{
		id:        ScheduleID(uuid.New()),
		alias:     alias,
		startTime: startTime,
		endTime:   endTime,
	}
}

func (s CustomSchedule) String() string {
	// short
	return fmt.Sprintf(
		"%s",
		s.Alias(),
	)

	// long
	//return fmt.Sprintf(
	//	"[%s, %s ~ %s]",
	//	s.Alias(),
	//	s.startTime.Format(layoutISO),
	//	s.endTime.Format(layoutISO),
	//)
}

func (s *CustomSchedule) ID() ScheduleID {
	return s.id
}

func (s *CustomSchedule) Alias() string {
	return s.alias
}

func (s *CustomSchedule) StartTime() time.Time {
	return s.startTime
}

func (s *CustomSchedule) EndTime() time.Time {
	return s.endTime
}

func (s *CustomSchedule) ScheduleType() ScheduleType {
	return Custom
}

type ScheduleSet map[ScheduleID]Schedule

func (set *ScheduleSet) AddSchedule(s Schedule) {
	(*set)[s.ID()] = s
}
