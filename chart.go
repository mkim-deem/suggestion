package main

import (
	"fmt"
	"github.com/go-echarts/go-echarts/v2/charts"
	"github.com/go-echarts/go-echarts/v2/components"
	"github.com/go-echarts/go-echarts/v2/opts"
	"io"
	"os"
)

func graphNodesFrom(g *Graph) []opts.GraphNode {
	nodes := make([]opts.GraphNode, 0)

	for _, gNode := range *g.nodeSet {
		newNode := opts.GraphNode{
			Name: fmt.Sprintf("%v", gNode.schedule),
		}

		nodes = append(nodes, newNode)
	}

	return nodes
}

func graphLinksFrom(g *Graph) []opts.GraphLink {
	links := make([]opts.GraphLink, 0)

	for _, edge := range *g.edgeSet {
		if !edge.isValid {
			continue
		}

		newLink := opts.GraphLink{
			Source: fmt.Sprintf("%v", edge.from.schedule),
			Target: fmt.Sprintf("%v", edge.to.schedule),
			Value:  0,
			LineStyle: &opts.LineStyle{
				Opacity: 1,
			},
		}
		links = append(links, newLink)
	}

	return links
}

func graphCircleFrom(g *Graph) *charts.Graph {
	graph := charts.NewGraph()

	nodes := graphNodesFrom(g)
	links := graphLinksFrom(g)

	graph.SetGlobalOptions(
		charts.WithTitleOpts(opts.Title{Title: "Circular layout"}),
	)

	graph.AddSeries("graph", nodes, links).
		SetSeriesOptions(
			charts.WithGraphChartOpts(
				opts.GraphChart{
					Force:      &opts.GraphForce{Repulsion: 8000},
					Layout:     "circular",
					EdgeSymbol: []string{"none", "arrow"},
				}),
			charts.WithLabelOpts(opts.Label{Show: true, Position: "right"}),
		)
	return graph
}

type GraphExamples struct{}

// Examples see more examples: https://github.com/go-echarts/examples
func (GraphExamples) Examples(g *Graph) {
	page := components.NewPage()
	page.AddCharts(
		//graphBase(),
		//graphCircle(),
		//graphNpmDep(),
		graphCircleFrom(g),
	)

	f, err := os.Create("result.html")
	if err != nil {
		panic(err)

	}
	page.Render(io.MultiWriter(f))
}
