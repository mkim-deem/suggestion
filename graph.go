package main

import (
	"fmt"
	"github.com/google/uuid"
)

type GraphNodeID uuid.UUID
type GraphEdgeID uuid.UUID

type NodeSet map[GraphNodeID]*Node

type Graph struct {
	nodeSet *NodeSet
	edgeSet *EdgeSet
}

func (id GraphNodeID) HexString() string {
	return fmt.Sprintf("%x", id)
}

func (g *Graph) String() string {
	log := ""

	log += "Nodes:" + newline
	log += fmt.Sprintf("%v", g.nodeSet)
	log += newline

	log += "Edges:" + newline
	log += fmt.Sprintf("%v", g.edgeSet)

	return log
}

func (g *Graph) Init(s *ScheduleSet) {
	for _, schedule := range *s {
		newNodeID := GraphNodeID(uuid.New())
		newNode := NewGraphNode(newNodeID, schedule)

		for _, neighborNode := range *g.nodeSet {
			newToNeighborEdge := NewEdge(newNode, neighborNode)
			g.edgeSet.Add(newToNeighborEdge)

			neighborToNewEdge := NewEdge(neighborNode, newNode)
			g.edgeSet.Add(neighborToNewEdge)
		}

		g.nodeSet.Add(newNode)
	}
}

func (set *NodeSet) Add(n *Node) {
	(*set)[n.id] = n
}

func (set NodeSet) String() string {
	log := ""
	for _, node := range set {
		log += node.String()
		log += newline
	}
	return fmt.Sprintf(log)
}

type EdgeSet map[GraphEdgeID]*Edge

func (set EdgeSet) String() string {
	log := ""
	for _, edge := range set {
		log += edge.String()
		log += newline
	}
	return fmt.Sprintf(log)
}

func (set *EdgeSet) Add(e *Edge) {
	(*set)[e.id] = e
}

func NewGraph() *Graph {
	nodeSet := make(NodeSet)
	edgeSet := make(EdgeSet)
	return &Graph{
		nodeSet: &nodeSet,
		edgeSet: &edgeSet,
	}
}

type Node struct {
	id       GraphNodeID
	schedule Schedule
}

func (n *Node) String() string {
	return fmt.Sprintf("GraphNode %x, %v", n.id, n.schedule)
}

func NewGraphNode(id GraphNodeID, s Schedule) *Node {
	return &Node{
		id:       id,
		schedule: s,
	}
}

type Edge struct {
	id      GraphEdgeID
	from    *Node
	to      *Node
	isValid bool
}

func (e *Edge) String() string {
	return fmt.Sprintf(
		"[Edge %x %t] %x -> %x",
		e.id,
		e.isValid,
		e.from,
		e.to,
	)
}

func NewEdge(from *Node, to *Node) *Edge {
	return &Edge{
		id:      GraphEdgeID(uuid.New()),
		from:    from,
		to:      to,
		isValid: true,
	}
}

func (g *Graph) FilterEdgesForSuggestions() {
	for _, edge := range *g.edgeSet {
		// filtered edges by the suggestion logic
		edge.isValid = true

		if !(edge.to.schedule.EndTime().After(edge.from.schedule.StartTime())) {
			edge.isValid = false
		}

		if edge.from.schedule.ScheduleType() == Custom && edge.to.schedule.ScheduleType() == Custom {
			if (edge.from.schedule.StartTime().Year() != edge.to.schedule.StartTime().Year()) ||
				(edge.from.schedule.StartTime().Day() != edge.to.schedule.StartTime().Day()) {
				edge.isValid = false
			}
		}

		if edge.from.schedule.ScheduleType() == Custom && edge.to.schedule.ScheduleType() == Flight {
			if (edge.from.schedule.StartTime().Year() != edge.to.schedule.StartTime().Year()) ||
				(edge.from.schedule.StartTime().Day() != edge.to.schedule.StartTime().Day()) {
				edge.isValid = false
			}
		}
	}
}
